
import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import org.hl7.fhir.dstu3.model.SearchParameter;
	
public class LoadSearchParameters {

	
	public static void main(String[] args) {
		FhirContext ctx = FhirContext.forDstu3();
		IGenericClient client = ctx.newRestfulGenericClient("http://localhost:8080/hapi/baseDstu3");

		SearchParameter sp = new SearchParameter();		
		
		// subject-identifier
		sp.addBase("Observation");
		sp.setCode("subject-identifier");
		sp.setType(org.hl7.fhir.dstu3.model.Enumerations.SearchParamType.TOKEN);
		sp.setUrl("http://hl7.org/fhir/SearchParameter/observation-subject-identifier");
		sp.setTitle("Observation subject.identifier Search Parameter");
		sp.setExpression("Observation.subject.identifier");
		sp.setXpathUsage(org.hl7.fhir.dstu3.model.SearchParameter.XPathUsageType.NORMAL);
		sp.setStatus(org.hl7.fhir.dstu3.model.Enumerations.PublicationStatus.ACTIVE);
		sp.addTarget("Patient");
		
		client
		.create()
		.resource(sp)
		.execute();
		
		// context-identifier
		sp = new SearchParameter();
		sp.addBase("Observation");
		sp.setCode("context-identifier");
		sp.setType(org.hl7.fhir.dstu3.model.Enumerations.SearchParamType.TOKEN);
		sp.setUrl("http://hl7.org/fhir/SearchParameter/observation-context-identifier");
		sp.setTitle("Observation context.identifier Search Parameter");
		sp.setExpression("Observation.context.identifier");
		sp.setXpathUsage(org.hl7.fhir.dstu3.model.SearchParameter.XPathUsageType.NORMAL);
		sp.setStatus(org.hl7.fhir.dstu3.model.Enumerations.PublicationStatus.ACTIVE);
		sp.addTarget("Patient");
		
		client
		.create()
		.resource(sp)
		.execute();		
		
		// context-type
		sp = new SearchParameter();
		sp.addBase("Observation");
		sp.setCode("context-type");
		sp.setType(org.hl7.fhir.dstu3.model.Enumerations.SearchParamType.TOKEN);
		sp.setUrl("http://hl7.org/fhir/SearchParameter/observation-context-type");
		sp.setTitle("Observation context.identifier.type Search Parameter");
		sp.setExpression("Observation.context.identifier.type");
		sp.setXpathUsage(org.hl7.fhir.dstu3.model.SearchParameter.XPathUsageType.NORMAL);
		sp.setStatus(org.hl7.fhir.dstu3.model.Enumerations.PublicationStatus.ACTIVE);
		sp.addTarget("Patient");
		
		client
		.create()
		.resource(sp)
		.execute();	
		
		// perfomer-identifier
		sp = new SearchParameter();
		sp.addBase("Observation");
		sp.setCode("performer-identifier");
		sp.setType(org.hl7.fhir.dstu3.model.Enumerations.SearchParamType.TOKEN);
		sp.setUrl("http://hl7.org/fhir/SearchParameter/observation-performer-identifier");
		sp.setTitle("Observation performer.identifier Search Parameter");
		sp.setExpression("Observation.perfomer.identifier");
		sp.setXpathUsage(org.hl7.fhir.dstu3.model.SearchParameter.XPathUsageType.NORMAL);
		sp.setStatus(org.hl7.fhir.dstu3.model.Enumerations.PublicationStatus.ACTIVE);
		sp.addTarget("Patient");
		
		client
		.create()
		.resource(sp)
		.execute();			
		
		// performer-type
		sp = new SearchParameter();
		sp.addBase("Observation");
		sp.setCode("performer-type");
		sp.setType(org.hl7.fhir.dstu3.model.Enumerations.SearchParamType.TOKEN);
		sp.setUrl("http://hl7.org/fhir/SearchParameter/observation-performer-type");
		sp.setTitle("Observation performer.identifier.type Search Parameter");
		sp.setExpression("Observation.performer.identifier.type");
		sp.setXpathUsage(org.hl7.fhir.dstu3.model.SearchParameter.XPathUsageType.NORMAL);
		sp.setStatus(org.hl7.fhir.dstu3.model.Enumerations.PublicationStatus.ACTIVE);
		sp.addTarget("Patient");
		
		client
		.create()
		.resource(sp)
		.execute();			
	}

}
