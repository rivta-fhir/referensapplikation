import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.parser.StrictErrorHandler;
import org.hl7.fhir.instance.model.api.IBaseResource;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FhirXmlFileLoader {

	public static <T extends IBaseResource> List<T> loadFromDirectory(String rootDir) {


		IParser xmlParser = FhirContext.forDstu3().newXmlParser();
		xmlParser.setParserErrorHandler(new StrictErrorHandler());
		List<T> definitions = new ArrayList<>();
		File[] profiles =
			new File(rootDir).listFiles();

		Arrays.asList(profiles).forEach(f -> {
			try {
				System.out.println(f);
				T sd = (T) xmlParser.parseResource(new FileReader(f));
				definitions.add(sd);
			} catch (FileNotFoundException e) {
				throw new RuntimeException(e);
			}
		});

		return definitions;
	}


}
