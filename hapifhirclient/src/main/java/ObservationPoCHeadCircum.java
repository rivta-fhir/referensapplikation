import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.validation.FhirValidator;
import ca.uhn.fhir.validation.ValidationResult;
import org.hl7.fhir.dstu3.hapi.ctx.IValidationSupport;
import org.hl7.fhir.dstu3.hapi.validation.DefaultProfileValidationSupport;
import org.hl7.fhir.dstu3.hapi.validation.FhirInstanceValidator;
import org.hl7.fhir.dstu3.hapi.validation.ValidationSupportChain;
import org.hl7.fhir.dstu3.model.*;

import java.io.IOException;
import java.util.Collections;

public class ObservationPoCHeadCircum {


	public static void main(String[] args) throws IOException {


		// Create an observation object
		Observation observation = new Observation();
		observation.setMeta(new Meta().addProfile("http://rivta.se/fhir/StructureDefinition/core/se-core-vitalsigns-v1/HeadCircum").setVersionId("1.0"));

		// meta.security: ApprovedForPatient
				observation.getMeta().addSecurity("http://hl7.org/fhir/v3/ActReason", "PATRQT", "ApprovedForPatient");
				
				//context: encounter: lokalt id för CareContact
				Coding encounterCoding = new Coding();
				encounterCoding.setCode("Encounter");
				encounterCoding.setSystem("http://hl7.org/fhir/resource-types");
				encounterCoding.setDisplay("Vård- och omsorgskontakt - lokalt id");
				observation.setContext(new Reference().setIdentifier(new Identifier().setType(new CodeableConcept().addCoding(encounterCoding)).setSystem("SE162321000065-B08").setValue("1111444")));

		observation.addCategory()
			.addCoding().setSystem("http://hl7.org/fhir/observation-category")
			.setCode("vital-signs");

		//Performer: Organisationsenhet: hsid för orgenhet
		Coding orgCod = new Coding();
		orgCod.setCode("PRN");
		orgCod.setSystem("http://hl7.org/fhir/v2/0203");
		orgCod.setDisplay("organisationsenhet");
		observation.addPerformer().setIdentifier(new Identifier().setType(new CodeableConcept().addCoding(orgCod)).setSystem("urn:oid:1.2.752.129.2.1.4.1")
			.setValue("3211444"));

		//Performer: Vårdgivare: HSAid för Vårdgivare
		Coding vgCod = new Coding();
		vgCod.setCode("EN");
		vgCod.setSystem("http://hl7.org/fhir/v2/0203");
		vgCod.setDisplay("Vårdgivare");
		observation.addPerformer().setIdentifier(new Identifier().setType(new CodeableConcept().addCoding(vgCod)).setSystem("urn:oid:1.2.752.129.2.1.4.1")
			.setValue("2222"));
		
		//Performer: Vårdenhet:  HSAid för Vårdenhet
		Coding veCod = new Coding();
		veCod.setCode("XX");
		veCod.setSystem("http://hl7.org/fhir/v2/0203");
		veCod.setDisplay("Vårdenhet");
		observation.addPerformer().setIdentifier(new Identifier().setType(new CodeableConcept().addCoding(veCod)).setSystem("urn:oid:1.2.752.129.2.1.4.1")
			.setValue("4444"));
		
		//Performer: Patient som registrerade obervationen (vid egenmätning - t.ex. mha sensor): Personidentitet
		Coding pnrCod= new Coding();
		pnrCod.setCode("PN");
		pnrCod.setSystem("http://hl7.org/fhir/v2/0203");
		pnrCod.setDisplay("Personnummer");
		observation.addPerformer().setIdentifier(new Identifier().setType(new CodeableConcept().addCoding(pnrCod))
			.setSystem("urn:oid:1.2.752.129.2.1.4.1")
			.setValue("1912121212"));
		
		//Observationens värde och måttenhet
		observation.setValue(new Quantity().setValue(173)
			.setUnit("cm")
			.setSystem("http://unitsofmeasure.org")
			.setCode("cm")

		);
		
		//Observationens kliniska kodning
		observation.setCode(new CodeableConcept().addCoding(new Coding().setCode("363812007")
			.setSystem("http://snomed.info/sct")));
		

		// source (extention): Källsystemets HSA-id
		Extension statusExt = new Extension();
		statusExt.setUrl("http://rivta.se/fhir/StructureDefinition/core/se-core-extentions/Source-v1");
		statusExt.setValue(new StringType("SE162321000065-B08"));
		observation.addExtension(statusExt);

		//subject: Observerad patient: personidentitet  
		observation.setSubject(new Reference().setIdentifier(new Identifier().setValue("191212121212")
			.setSystem("urn:oid:1.2.752.129.2.1.3.1")));
		
		//Observationstidpunkt
		observation.setEffective(new DateTimeType(String.valueOf(2013 - 04 - 02)));
		
		//Observationsstatus
		observation.setStatus(Observation.ObservationStatus.FINAL);

		// Create a bundle that will be used as a transaction
		Bundle bundle = new Bundle();
		bundle.setType(Bundle.BundleType.TRANSACTION);


		// Add the observation. This entry is a POST with no header
		// (normal create) meaning that it will be created even if
		// a similar resource already exists.
		bundle.addEntry()
			.setResource(observation)
			.getRequest()
			.setUrl("Observation")
			.setMethod(Bundle.HTTPVerb.POST);

		// Log the request
		FhirContext ctx = FhirContext.forDstu3();
		System.out.println(ctx.newXmlParser().setPrettyPrint(true).encodeResourceToString(bundle));

		// Validera
		/*
		FhirValidator validator = ctx.newValidator();

		FhirInstanceValidator instanceValidator = new FhirInstanceValidator();
		IValidationSupport valSupport = new ObservationValidationSupport("profiles/PoCHeadCircum");
		ValidationSupportChain support = new ValidationSupportChain(new DefaultProfileValidationSupport(), valSupport);
		instanceValidator.setValidationSupport(support);


		validator.registerValidatorModule(instanceValidator);

		ValidationResult validationResult = validator.validateWithResult(observation);

		OperationOutcome operationOutcome = (OperationOutcome) validationResult.toOperationOutcome();
		IParser parser = ctx.newXmlParser().setPrettyPrint(true);
		System.out.println(parser.encodeResourceToString(operationOutcome));
		 */

		// Create a client and post the transaction to the server
		IGenericClient client = ctx.newRestfulGenericClient("http://localhost:8080/hapi/baseDstu3");
		Bundle resp = client.transaction().withBundle(bundle).execute();

// Log the response
		System.out.println(ctx.newXmlParser().setPrettyPrint(true).encodeResourceToString(resp));

	}


}
