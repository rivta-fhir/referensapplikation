

### Referensapplikation för FHIR POC  

För att köra HAPI server behöver du checka ut apache-tomcat-9.0.4. Sen kör bin/startup.bat för att starta applikationservern.

Server sidan :
HAPI FHIR version :DSTU3 3.2.0
Applikationserver: apache-tomcat-9.0.4

Klient-applikationen består av 4 main-klasser som skapar resurser i hapi-servern. Dessa resurser kan sedan sökas med hjälp av FHIR REST-URL:er (search) direkt i webbläsare eller utforskande via en test-applikation som ingår i HAPI och som publiceras av HAPI-servern. Efter start av HAPI-servern enligt ovan, kan testapplikationen nås på denna URL:

http://localhost:8080/hapi/baseDstu3

Efter start är HAPI-servern tom. För att ladda in testdata behöver main-klasserna köras. De behöver köras i följande ordning:

1 LoadSearchParameters
2 ObservationPoCBodyheight
2 ObservationPoCBodyWeight
2 ObservationPoCHeadCircum

